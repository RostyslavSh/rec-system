from rest_framework.views import APIView
from .serializers import RatingSerializer
from rest_framework.response import Response
from rest_framework import status
from ratings.als_engine import RecommendationEngine


class RatingsListCreateView(APIView):

    serializer_class = RatingSerializer

    def post(self, request, format=None):
        serializer = RatingSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class PredictView(APIView):

    def get(self, request, user_id, movie_id, format=None):
        recommendation_engine = RecommendationEngine()
        ratings = recommendation_engine.get_ratings_for_movie_ids(user_id, [movie_id])
        response = []
        for rating in ratings:
            response.append({'movie_id': rating[0], 'predicted_rating': rating[1]})
        return Response(response)
