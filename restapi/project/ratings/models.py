from mongoengine import Document, fields


class Rating(Document):
    movie_id = fields.IntField()
    user_id = fields.IntField()
    rating = fields.FloatField()
    timestamp = fields.IntField()
