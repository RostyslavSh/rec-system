from django.urls import path
from .views import RatingsListCreateView, PredictView

app_name = 'ratings'

urlpatterns = [
    path('', RatingsListCreateView.as_view(), name="list_create"),
    path('predict/<int:user_id>/<int:movie_id>', PredictView.as_view(), name="predict")
]