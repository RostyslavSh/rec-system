import os
import shutil
from pyspark.mllib.recommendation import ALS
from pyspark.mllib.recommendation import MatrixFactorizationModel
import logging
from pyspark import SparkContext, SparkConf
from pyspark.sql import SparkSession


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


def get_counts_and_averages(id_and_ratings_tuple):
    """Given a tuple (movieID, ratings_iterable)
    returns (movieID, (ratings_count, ratings_avg))
    """
    nratings = len(id_and_ratings_tuple[1])
    return id_and_ratings_tuple[0], (nratings, float(sum(x for x in id_and_ratings_tuple[1])) / nratings)


class RecommendationEngine:
    """A movie recommendation engine
    """

    def __count_and_average_ratings(self):
        """Updates the movies ratings counts from
        the current data self.ratings_rdd
        """
        logger.info("Counting movie ratings...")
        movie_id_with_ratings_rdd = self.ratings_rdd.map(lambda x: (x[1], x[2])).groupByKey()
        movie_id_with_avg_ratings_rdd = movie_id_with_ratings_rdd.map(get_counts_and_averages)
        self.movies_rating_counts_RDD = movie_id_with_avg_ratings_rdd.map(lambda x: (x[0], x[1][0]))

    def __predict_ratings(self, user_and_movie_rdd):
        """Gets predictions for a given (userID, movieID) formatted RDD
        Returns: an RDD with format (movieTitle, movieRating, numRatings)
        """
        predicted_rdd = self.model.predictAll(user_and_movie_rdd)
        predicted_rating_rdd = predicted_rdd.map(lambda x: (x.product, x.rating))
        predicted_rating_title_and_count_rdd = \
            predicted_rating_rdd.join(self.movies_rating_counts_RDD)
        predicted_rating_title_and_count_rdd = \
            predicted_rating_title_and_count_rdd.map(lambda r: (r[0], r[1][0], r[1][1]))
        return predicted_rating_title_and_count_rdd

    def get_ratings_for_movie_ids(self, user_id, movie_ids):
        """Given a user_id and a list of movie_ids, predict ratings for them
        :returns A tuple (movie_id, rating, ratings_count)
        """
        requested_movies_rdd = self.sc.parallelize(movie_ids).map(lambda x: (user_id, x))
        # Get predicted ratings
        ratings = self.__predict_ratings(requested_movies_rdd).collect()

        return ratings

    def get_top_ratings(self, user_id, movies_count):
        """Recommends up to movies_count top unrated movies to user_id
        :returns A tuple (movie_id, rating, ratings_count)
        """
        # Get pairs of (userID, movieID) for user_id unrated movies
        user_unrated_movies_rdd = self.ratings_rdd.filter(lambda rating: not rating[0] == user_id) \
            .map(lambda x: (user_id, x[1])).distinct()
        # Get predicted ratings
        ratings = self.__predict_ratings(user_unrated_movies_rdd).filter(lambda r: r[2] >= 25).takeOrdered(movies_count,
                                                                                                           key=lambda
                                                                                                               x: -x[1])

        return ratings

    def __init__(self):
        """Init the recommendation engine given a Spark context and a dataset path
        """
        self.dataset_path = '../research_data/'
        conf = SparkConf().setAppName("movie_recommendation-server")
        self.sc = SparkContext.getOrCreate(conf=conf)
        
        logger.info("Starting up the Recommendation Engine: ")

        # Load ratings data for later use
        logger.info("Loading Ratings data...")
        ratings_file_path = os.path.join(self.dataset_path, 'ratings.csv')
        ratings_raw_rdd = self.sc.textFile(ratings_file_path)
        ratings_raw_data_header = ratings_raw_rdd.take(1)[0]
        self.ratings_rdd = ratings_raw_rdd.filter(lambda line: line != ratings_raw_data_header) \
            .map(lambda line: line.split(",")).map(
            lambda tokens: (int(tokens[0]), int(tokens[1]), float(tokens[2]))).cache()
        # Pre-calculate movies ratings counts
        self.__count_and_average_ratings()

        manager = AlsManager(self.sc)
        self.model = manager.get()


class AlsManager:
    """Manages als model
    """

    def __init__(self, sc):
        self.dataset_path = '../research_data/'
        self.save_path = '../research_data/ml_models/ratings'
        self.model = None
        self.sc = sc

    def get(self, mode='load'):
        """Get als trained model
       :returns bool
       """
        if mode == 'new':
            self.create_and_save()
        elif mode == 'load':
            if not self.__model_saved():
                self.create_and_save()
            self.__load()
        return self.model

    def create_and_save(self):
        if self.__model_saved():
            shutil.rmtree(self.save_path)
        self.__create()
        self.model.save(self.sc, path=self.save_path)

    def __model_saved(self):
        return os.path.isdir(self.save_path)

    def __create(self):
        """Create als trained model
        """
        # Train the model options
        rank = 8
        seed = 5
        iterations = 10
        regularization_parameter = 0.1
        mongo_session = SparkSession \
            .builder \
            .appName("myApp") \
            .config('spark.mongodb.input.uri', 'mongodb://root:example@mongo:27017/admin.rating') \
            .config('spark.mongodb.output.uri', 'mongodb://root:example@mongo:27017/admin.rating') \
            .config('spark.jars.packages', 'org.mongodb.spark:mongo-spark-connector_2.11:2.4.0') \
            .getOrCreate()
        ratings_collection = mongo_session.read.format("com.mongodb.spark.sql.DefaultSource").load()
        ratings_rdd = ratings_collection.rdd.map(
            lambda row: (row.user_id, row.movie_id, row.rating)).cache()
        model = ALS.train(ratings_rdd, rank, seed=seed,
                          iterations=iterations, lambda_=regularization_parameter)
        self.model = model

    def __load(self):
        """Load als trained model from disk
        :returns bool
        """
        model = MatrixFactorizationModel.load(self.sc, self.save_path)
        self.model = model
