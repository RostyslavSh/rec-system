from django.core.management.base import BaseCommand, CommandError
from ratings.als_engine import RecommendationEngine


class Command(BaseCommand):
    help = 'Test command'

    def handle(self, *args, **options):
        recommendation_engine = RecommendationEngine()
        ratings = recommendation_engine.get_top_ratings(1, 1)
        print(ratings)