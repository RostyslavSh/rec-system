from django.core.management.base import BaseCommand, CommandError
from ratings.als_engine import AlsManager
from pyspark import SparkContext, SparkConf


class Command(BaseCommand):
    help = 'Train model against ratings data'

    def handle(self, *args, **options):
        conf = SparkConf().setAppName("movie_recommendation-server")\
            .set('spark.executor.memory', '4G')\
            .set('spark.driver.memory', '4G')\
            .set('spark.driver.maxResultSize', '4G')
        sc = SparkContext(conf=conf, pyFiles=[])
        self.stdout.write('Initializing training engine...')
        manager = AlsManager(sc)
        manager.create_and_save()
        self.stdout.write('ALS Model is created and saved.')
        sc.stop()
