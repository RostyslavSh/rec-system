from rest_framework_mongoengine import serializers
from .models import Rating


class RatingSerializer(serializers.DocumentSerializer):
    class Meta:
        model = Rating
        fields = '__all__'

    def save(self, **kwargs):
        rating = Rating.objects(user_id=self.validated_data['user_id'], movie_id=self.validated_data['movie_id']).first()
        if rating is not None:
            rating.rating = self.validated_data['rating']
            rating.save()
        else:
            super().save(**kwargs)
