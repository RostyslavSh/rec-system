from django.db import models
from django.contrib.postgres.fields import JSONField


class Movie(models.Model):
    title = models.CharField(max_length=255)
    genres = JSONField()

    def __str__(self):
        return self.title
