from django.urls import path
from movies.views import TopMovies

app_name = 'movies'

urlpatterns = [
    path('top/<int:user_id>/<int:count>', TopMovies.as_view(), name="list"),
]
