from rest_framework import serializers
from movies.models import Movie


class MovieSerializer(serializers.Serializer):

    class Meta:
        model = Movie
        fields = ("id", "title")

    id = serializers.IntegerField()
    title = serializers.CharField()