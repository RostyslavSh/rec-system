from rest_framework.views import APIView
from rest_framework.response import Response
from ratings.als_engine import RecommendationEngine
from movies.models import Movie
from movies.serializers import MovieSerializer


class TopMovies(APIView):
    """
    Get top 10 movies.
    """
    authentication_classes = ()
    permission_classes = ()

    def get(self, request, user_id, count, format=None):
        recommendation_engine = RecommendationEngine()
        ratings = recommendation_engine.get_top_ratings(user_id, count)
        movie_ids = list(map(lambda x: x[0], ratings))
        movies = Movie.objects.filter(pk__in=movie_ids)
        serializer = MovieSerializer(movies, many=True)
        return Response(serializer.data)
